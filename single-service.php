<?php
get_header(); ?>
		<div id="primary" class="content-area">
			<main id="main" class="site-main services-page">

				<?php
				while ( have_posts() ) : the_post();
					?>
					<div class="container block">
					<?php
					get_template_part( 'template-parts/single-our-services-services' );
					?>
					</div>
					<?php

					?>
					<div>
					<?php
					get_template_part( 'template-parts/single-our-services-book-consultation' );
					?>
					</div>
					<?php

					?>
					
					<?php
					get_template_part( 'template-parts/single-our-services-current-projects' );
					?>
					<?php

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>


<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<footer id="colophon" class="site-footer">
		<div class="bg-gray-alt2 clearfix pt-md">
			<div class="footer-address"><?php get_template_part("/inc/address-card"); ?></div>
			<!--<div class="footer-utility"><?php echo do_shortcode('[lg-social-media]');?></div>-->
		</div>
		<div class="bg-gray-darker pb-lg pt-lg clearfix">
			<div class="site-info"><?php get_template_part("/inc/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/inc/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

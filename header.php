<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TSKZTVX');</script>
	<!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TSKZTVX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

<div class="slider-fixed">
	
	<a class="site-phone mb-xs" href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"> <i class="fa fa-phone" aria-hidden="true"></i> <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?> </a>
	
	<header id="masthead" class="site-header">
		<div>
			<div class="site-branding">
				<?php the_custom_logo(); ?>
				<p class="small text-center bg-primary mt-xs">Turning your vision and dreams into reality</p>
			</div><!-- .site-branding -->

			<?php get_template_part("/inc/nav-main"); ?>
		</div>

		<div class="util-phone">
			<?php get_template_part("/inc/util-phone"); ?>
		</div>
		
	</header><!-- #masthead -->
</div>

	<?php if (!is_front_page()) {
		get_template_part("/inc/featured-image");
	} ?>

<?php
  $feedback_cta_title = get_field('feedback_cta_title');
  $feedback_cta_button_link = get_field('feedback_cta_button_link');
  $feedback_cta_button = get_field('feedback_cta_button');
?>

<div class="bg-reverse-primmary-pattern block block-cta">
	<div class="container">
		<div>
			<div>
				<?php if($feedback_cta_title): ?>
					<?php echo $feedback_cta_title; ?>
				<?php endif; ?>
			</div>
			<a href="<?php echo $feedback_cta_button_link; ?>"><?php echo $feedback_cta_button; ?></a>
		</div>
	</div>
</div>
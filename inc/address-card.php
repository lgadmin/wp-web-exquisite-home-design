
<!-- Default Address Stuff -->
<div class="address-card">
	<h2><?php bloginfo( 'name' ); ?></h2>
	<!--<address itemscope="" itemtype="http://schema.org/LocalBusiness">
		<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress"><?php echo do_shortcode('[lg-address1]'); ?></span><br>
			<span itemprop="addressLocality"><?php echo do_shortcode('[lg-city]'); ?></span>, <span itemprop="addressRegion"><?php echo do_shortcode('[lg-province]'); ?></span>&nbsp;<span itemprop="postalCode"><?php echo do_shortcode('[lg-postcode]'); ?></span><br>
		</span>
	</address>-->
	<?php 
		$news = array();
		$args = array(
            'showposts'	=> -1,
            'post_type'		=> 'service',
        );
        $result = new WP_Query( $args );

        // Loop
        if ( $result->have_posts() ) :
        	?>
        	<ul class="footer-links">
        	<?php
            while( $result->have_posts() ) : $result->the_post();
        	?>
        		<li>
        			<a href="<?php echo get_permalink();?>"><?php the_title(); ?></a>
        		</li>
			<?php
            endwhile;
            ?>
            <li><a href="/about/">About</a></li>
            <li><a href="/contact/">Contact</a></li>
            </ul>
            <?php
        endif; // End Loop

        wp_reset_query();
    ?>
</div>


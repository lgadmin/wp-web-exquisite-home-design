<div class="header_phone">
	<?php $myphone =  do_shortcode('[lg-phone-main]'); ?>
	
	<a href="tel:+1<?php echo $myphone; ?>">
		<i class="fa fa-phone" aria-hidden="true"></i>
		<?php echo format_phone($myphone); ?>
	</a>
	
</div>



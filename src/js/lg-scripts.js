(function($) {
  $(document).ready(function(){
    $('.current-projects .tab a').on('click', function(){
    	var tab_content = $(this).attr('tab');
    	console.log(tab_content);

    	$(this).addClass('active').closest('li').siblings().find('a').removeClass('active');
    	$('.tab-content[tab="'+tab_content+'"]').addClass('active').siblings().removeClass('active');
    });

    if($('.single-service')[0] && !$('.menu-item-object-service').hasClass('active')){
    	$('.menu-item-object-service').addClass('active');
    }

    $('.project-gallery').slick({
      dots: false,
      arrows: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true
    });
 });
}(jQuery));
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
	<?php if (!is_front_page()) : ?>
		<div id="content">
	<?php endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
				$title = get_field('intro_title');
				$description = get_field('intro_description');
			?>
			<div class="gallery block container">
				<div class="row">
					<div class="col-md-4 col-sm-5">
						<?php if($title): ?>
							<h2><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php if($description): ?>
							<?php echo $description; ?>
						<?php endif; ?>

						<!-- Services Tabs  -->
						<?php 
							$args = array(
					            'showposts'	=> -1,
					            'post_type'		=> 'service',
					        );
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() ) :
					        	?>
								<ul class="tab">
					        	<?php
					            while( $result->have_posts() ) : $result->the_post();
					            	global $post;
									$post_slug=$post->post_name;
					        	?>
					        		<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
								<?php
					            endwhile;
					            ?>
								</ul>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
					    ?>

					</div>
					<div class="col-md-8 col-sm-7">
						<?php 
							$args = array(
					            'showposts'	=> -1,
					            'post_type'		=> 'lg_image_comparison',
					        );
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() || have_rows('additional_images') ) :
					        	?>
								<div class="project-gallery">
					        	<?php

					        	//Images from Before and After
					            while( $result->have_posts() ) : $result->the_post();
					            	$after_image = get_field('after_image');
					        	?>
					        		<div><img src="<?php echo $after_image; ?>" alt=""></div>
								<?php
					            endwhile;

					            wp_reset_query();

					            //Images from Master Sliders

					            //Add slider ID to exclude from display
					            $slider_to_exclude = [
					            	1
					            ];
								
								$sliders = get_mastersliders(0, 0, "ID", "ASC", "ID NOT IN ( '" . implode( "', '" , $slider_to_exclude ) . "' )");
								foreach ($sliders as $sliders_key => $sliders_value) {
									$slides = get_masterslider_parsed_data($sliders_value['ID'])['slides'];
									foreach ($slides as $slides_key => $slides_value) {
										?>
										<div><img src="<?php echo content_url() . '/uploads/' . $slides_value['src']; ?> " alt=""></div>
										<?php
									}
								}

								?>
								<?php

					            //Additional Images
							    while ( have_rows('additional_images') ) : the_row();
							        $image = get_sub_field('image');
							        ?>
									<div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
							        <?php
							    endwhile;

					            ?>
								</div>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
					    ?>
					</div>
				</div>
			</div>

			<?php

			//print_r(get_masterslider_parsed_data(1)['slides']);
?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php if (!is_front_page()) : ?>
		</div>
	<?php endif; ?>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>


<?php 
	$news = array();
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'project',
    );
    $current_post_id = get_the_ID();
    $find_project = false;
    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
        while( $result->have_posts() ) : $result->the_post();
        	global $post;
			$post_slug=$post->post_name;
			$parent_service = get_field('service');
    		if(is_array($parent_service) && in_array($current_post_id,$parent_service)){
    			$find_project = true;
			}
        endwhile;
    endif; // End Loop

    wp_reset_query();
?>


<?php if($find_project): ?>
<div class="container block">
	<?php
		$projects_intro_title = get_field('projects_intro_title');
	?>
	<div class="current-projects">
		<?php if($projects_intro_title): ?>
			<h2><?php echo $projects_intro_title; ?></h2>
			<hr>
		<?php endif; ?>

		<div class="project-details">
			<div>
				<!-- Project Tabs  -->
				<?php 
					$news = array();
					$args = array(
			            'showposts'	=> -1,
			            'post_type'		=> 'project',
			        );
			        $result = new WP_Query( $args );

			        // Loop
			        if ( $result->have_posts() ) :
			        	?>
						<ul class="tab">
			        	<?php
			        	$index = 0;
			            while( $result->have_posts() ) : $result->the_post();
			            	global $post;
							$post_slug=$post->post_name;

							$parent_service = get_field('service');
			        		if(is_array($parent_service) && in_array($current_post_id,$parent_service)){
			        	?>
			        		<li><a tab="<?php echo $post_slug; ?>" <?php if($index == 0){echo 'class="active"';} ?>><?php echo get_the_title(); ?></a></li>
						<?php
						$index++;
						}
			            endwhile;
			            ?>
						</ul>
			            <?php
			        endif; // End Loop

			        wp_reset_query();
			    ?>

			</div>
			<div>
				<div style="position:relative;">
					<!-- Project Tab Contents  -->
					<?php 
						$news = array();
						$args = array(
				            'showposts'	=> -1,
				            'post_type'		=> 'project',
				        );
				        $result = new WP_Query( $args );

				        // Loop
				        if ( $result->have_posts() ) :
				        	$index = 0;
				            while( $result->have_posts() ) : $result->the_post();
				            	global $post;
								$post_slug=$post->post_name;

								$parent_service = get_field('service');
			        			if(is_array($parent_service) && in_array($current_post_id,$parent_service)){

								$title = get_the_title();
								$dead_line = get_field('project_deadline');
								$description = get_field('project_description');
								
				        	?>
				        		<div class="tab-content project-contents row <?php if($index == 0){echo 'active';} ?>" tab="<?php echo $post_slug; ?>" >
				        			<div class="col-md-7">
				        				<div style="position:relative;">
				        				<?php
				        					// check if the repeater field has rows of data
											if( have_rows('image_comparison') ):
												$index = 0;
											 	// loop through the rows of data
											    while ( have_rows('image_comparison') ) : the_row();
											        // display a sub field value
											        $shortcode = get_sub_field('shortcode');
											        ?>
											        <div class="tab-content <?php if($index == 0){echo 'active';} ?>" tab="project-image-comparison-<?php echo $index; ?>">
											        <?php
											        echo do_shortcode($shortcode);
											        ?>
											        </div>
											        <?php
											        $index++;
											    endwhile;
											else :
											    // no rows found
											endif;

											// check if the repeater field has rows of data
											if( have_rows('image_comparison') ):
												$index = 0;
												?>
												<ul class="tab">
												<?php
											 	// loop through the rows of data
											    while ( have_rows('image_comparison') ) : the_row();
											        // display a sub field value
											        $thumbnail = get_sub_field('thumbnail');
											        ?>
													<li><a href="javascript:void(0)" tab="project-image-comparison-<?php echo $index; ?>" <?php if($index == 0){echo 'class="active"';} ?>><img src="<?php echo $thumbnail; ?>" alt=""></a></li>
											        <?php
											    $index++;
											    endwhile;
											    ?>
												</ul>
											    <?php
											else :
											    // no rows found
											endif;
				        				?>
				        				</div>
				        			</div>

				        			<div class="col-md-5">
				        				<?php if($title): ?>
										<h2><?php echo $title; ?></h2>
										<?php endif; ?>

										<?php if($dead_line): ?>
											<h3 class="deadline">Deadline: <?php echo $dead_line; ?></h3>
										<?php endif; ?>

										<?php if($description): ?>
										<div class="description">
											<?php echo $description; ?>
										</div>
										<?php endif; ?>

										<?php

										// check if the repeater field has rows of data
										if( have_rows('project_documents') ):
											?>
											<div class="docs">
											<?php
										 	// loop through the rows of data
										    while ( have_rows('project_documents') ) : the_row();
										        // display a sub field value
										        $file_title = get_sub_field('title');
										        $file_url = get_sub_field('document');
										        ?>
												<div>
													<i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <a href="<?php echo $file_url; ?>"><?php echo $file_title; ?></a>
												</div>
										        <?php
										    endwhile;
										    ?>
											</div>
										    <?php
										else :
										    // no rows found
										endif;

										?>
				        			</div>
				        		</div>
							<?php
							$index++;
							}
				            endwhile;
				        endif; // End Loop

				        wp_reset_query();
				    ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
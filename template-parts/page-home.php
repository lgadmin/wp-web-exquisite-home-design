<?php 
	function home_service_description_format($string){
		$return_string = '';
		$char_limit = 50;
		$string = strip_tags($string);
		return substr($string,0,$char_limit);
	}
?>

<?php
	$intro_title = get_field('intro_title');
	$intro_description = get_field('intro_content');
?>
<div class="bg-gray">
	<div class="intro-block container">
		<?php if($intro_title): ?>
		<h2><?php echo $intro_title; ?></h2>
		<?php endif; ?>
		<hr>
		<?php if($intro_description): ?>
		<?php echo $intro_description; ?>
		<?php endif; ?>
	</div>
</div>

<?php
	$service_title = get_field('services_title');
	$service_description = get_field('services_description');
?>

<div class="bg-white">
	<div class="intro-block container">
		<h2>Services</h2>
		<hr>

		<ul class="home-services">
			<li>
				<?php if($service_title): ?>
					<h2><?php echo $service_title; ?></h2>
				<?php endif; ?>
				<?php if($service_description): ?>
					<?php echo $service_description; ?>
				<?php endif; ?>
			</li>
		<!-- Services  -->
			<?php 
				$news = array();
				$args = array(
		            'showposts'	=> -1,
		            'post_type'		=> 'service',
		        );
		        $result = new WP_Query( $args );

		        // Loop
		        if ( $result->have_posts() ) :
		        	?>
		        	<?php
		            while( $result->have_posts() ) : $result->the_post();
		            $service_description = get_field('service_description');

		            $service_description = home_service_description_format($service_description);
		        	?>
		        		<li>
		        			<div class="home-services-image">
		        				<?php the_post_thumbnail(); ?>
		        				<div class="overlay"></div>
		        			</div>
		        			<h2><?php the_title(); ?></h2>
		        			<p><?php echo $service_description; ?>...</p>
		        			<a class="cta" href="<?php echo get_permalink();?>">Read More</a>
		        		</li>
					<?php
		            endwhile;
		            ?>
		            <?php
		        endif; // End Loop

		        wp_reset_query();
		    ?>
		</ul>
	</div>
</div>
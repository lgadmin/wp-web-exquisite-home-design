<?php
	$book_consultation_title = get_field('book_consultation_title');
	$book_consultation_link = get_field('book_consultation_link');
	$book_consultation_cta = get_field('book_consultation_cta');
?>
<div class="book-consultation block block-cta">
	<div class="container">
		<div>
			<?php if($book_consultation_title): ?>
			<h2><?php echo $book_consultation_title; ?></h2>
			<?php endif; ?>
			<a href="<?php echo $book_consultation_link; ?>"><?php echo $book_consultation_cta; ?></a>
		</div>
	</div>
</div>
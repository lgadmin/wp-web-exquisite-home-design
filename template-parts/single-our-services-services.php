<?php
	$service_intro_title = get_field('service_intro_title');
	$service_intro_description = get_field('service_intro_description');
	$current_post_id = get_the_ID();
?>
<div class="services">
	<div class="row">
		<div class="col-md-4 col-sm-5">
			<?php if($service_intro_title): ?>
				<h2><?php echo $service_intro_title; ?></h2>
			<?php endif; ?>
			<?php if($service_intro_description): ?>
				<?php echo $service_intro_description; ?>
			<?php endif; ?>

			<!-- Services Tabs  -->
			<?php 
				$news = array();
				$args = array(
		            'showposts'	=> -1,
		            'post_type'		=> 'service',
		        );
		        $result = new WP_Query( $args );

		        // Loop
		        if ( $result->have_posts() ) :
		        	?>
					<ul class="tab">
		        	<?php
		            while( $result->have_posts() ) : $result->the_post();
		            	global $post;
						$post_slug=$post->post_name;
		        	?>
		        		<li><a href="<?php echo get_permalink(); ?>" <?php if($current_post_id == get_the_ID()){echo 'class="active"';} ?>><?php echo get_the_title(); ?></a></li>
					<?php
		            endwhile;
		            ?>
					</ul>
		            <?php
		        endif; // End Loop

		        wp_reset_query();
		    ?>

		</div>
		<div class="col-md-8 col-sm-7">
			<div style="position:relative;">
				<!-- Services Tab Contents  -->
				<?php 
					$title = get_the_title();
					$description = get_field('service_description');
					$slider = get_field('image_slider');
        		?>
        		<div class="tab-content <?php if($index == 0){echo 'active';} ?>" tab="<?php echo $post_slug; ?>" >
					<?php if($slider): ?>
						<?php echo do_shortcode($slider); ?>
					<?php endif; ?>

					<div class="description">
						<?php if($title): ?>
							<h2><?php echo $title; ?></h2>
	        			<?php endif; ?>

	        			<?php if($description): ?>
							<?php echo $description; ?>
	        			<?php endif; ?>
					</div>
        		</div>
			</div>
		</div>
	</div>
</div>

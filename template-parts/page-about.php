<?php 
  $intro_title = get_field('intro_title');
  $intro_description = get_field('intro_description');
  $intro_cta_text = get_field('intro_cta_text');
  $intro_cta_link = get_field('intro_cta_link');
  $intro_image = get_field('intro_image');
  $our_clients_headline = get_field('our_clients_headline');
  $clients_carousel = get_field('clients_carousel');
?>

<div class="block container">
	<div class="split-content reverse">
		<div class="half-image">
			<?php if($intro_image): ?>
			<img src="<?php echo $intro_image; ?>" alt="">
			<?php endif; ?>
		</div>
		<div class="half-copy">
			<?php if($intro_title): ?>
				<?php echo $intro_title; ?>
			<?php endif; ?>
			<?php if($intro_description): ?>
				<?php echo $intro_description; ?>
			<?php endif; ?>
			<p class="about-email"><a href="mailto:<?php echo do_shortcode('[lg-email]') ?>"><i class="fa fa-envelope" aria-hidden="true"></i>  <span><?php echo do_shortcode('[lg-email]') ?></span></a></p>
			<?php if($intro_cta_text && $intro_cta_link): ?>
				<p>
					<a class="cta" href="<?php echo $intro_cta_link; ?>"><?php echo $intro_cta_text; ?></a>
				</p>
			<?php endif; ?>
		</div>
	</div>
</div>

<!--<div class="bg-gray">
	<section class="cta-testimonials">
		<div class="container-fluid">
			<div class="container">
				<?php echo do_shortcode('[hms_testimonials_rotating template="7" order="testimonial_date" direction="ASC"]') ?>
			</div>
		</div>
	</section>   
</div>-->


<!--<div class="intro-block container">
	<?php if($our_clients_headline): ?>
		<h2><?php echo $our_clients_headline; ?></h2>
		<hr>
	<?php endif; ?>	

	<?php if($clients_carousel): ?>
		<?php echo do_shortcode($clients_carousel); ?>
	<?php endif; ?>	
</div>-->
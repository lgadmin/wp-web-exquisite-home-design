<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */
 ?>
 <?php get_header(); ?>

<div class="container-fluid home-slider">
	<?php echo do_shortcode("[masterslider id='1']"); ?>
	<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
</div>

<div id="content">  


	<div id="primary" class="content-area">
		<main id="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part("/template-parts/page-home");
				get_template_part("/inc/cata-reverse");
				//get_template_part("/inc/cta-testimonial-rotator");

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- #content -->

<?php get_footer(); ?>


<?php

	// Scripts and Styles
	function lg_enqueue_styles_scripts() {
	    
	    // Child theme style sheet setup.
	    $parent_style = '_s-style';
	    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), '1.0.1' );

		// Fonts
		wp_enqueue_style('wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700|Roboto:100,300,300i,400,400i,700', false );
		wp_enqueue_style('slickcss', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', false );
		wp_enqueue_style('slickthemecss', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', false);


		// Register Scripts
		wp_register_script( 'bootstrapjs', get_stylesheet_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.7', true );
		wp_register_script( 'lg-scripts', get_stylesheet_directory_uri() . "/js/lg-scripts-min.js", array('jquery'), '0.0.1', true );
		wp_register_script( 'slickjs', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js", [], '1.0.0', true );
 
		// Enqueue Scripts.
		wp_enqueue_script( 'bootstrapjs' );
		wp_enqueue_script( 'lg-scripts' );
		wp_enqueue_script( 'slickjs' );
	}
	add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );

	//Dequeue JavaScripts
	function project_dequeue_unnecessary_scripts() {
		wp_dequeue_script( '_s-navigation' );
		wp_deregister_script( '_s-navigation' );
	}
	add_action( 'wp_print_scripts', 'project_dequeue_unnecessary_scripts' );

	// Format Phone Numbers Function
	function format_phone($phone)
	{
		$phone = preg_replace("/[^0-9]/", "", $phone);
	 
		if(strlen($phone) == 7)
			return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
		elseif(strlen($phone) == 10)
			return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
		else
			return $phone;
	}

	// Register Custom Navigation Walker
	require_once get_stylesheet_directory() . '/inc/wp-bootstrap-navwalker.php';


	// Custom Post Types
	function create_post_type() {
	  
	  // SERVICES
	  register_post_type( 'service',
	    array(
	        'labels' => array(
	        'name' => __( 'Service' ),
	        'singular_name' => __( 'Service' )
	      ),
	      'public' => true,
	      'has_archive' => false,
	      'hierarchical' => true,
	      'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', ),
	    )
	  );

	  // PROJECT
	  register_post_type( 'project',
	    array(
	        'labels' => array(
	        'name' => __( 'Project' ),
	        'singular_name' => __( 'Project' )
	      ),
	      'show_ui' => true,
	      'show_in_menu' => true,
	      'has_archive' => false,
	      'hierarchical' => true,
	      'supports' => array( 'title', 'editor', 'thumbnail', 'revisions'),
	    )
	  );

	}
	add_action( 'init', 'create_post_type' );

	function wpcodex_add_excerpt_support_for_pages() {
		add_post_type_support( 'service', 'revisions' );
	}
	add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );

	add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

	function main_nav_items ( $items, $args ) {
	    if ($args->menu == 'primary') {
	        $items .= '<li class="mobile-header-phone"><a href="tel:'.do_shortcode('[lg-phone-main]').'"><i class="fa fa-phone" aria-hidden="true"></i>   '.format_phone(do_shortcode('[lg-phone-main]')).'</a></li>';
	    }
	    return $items;
	}



?>